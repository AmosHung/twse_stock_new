import datetime

LogFilePath = "../FinanceData/LogFile.txt"
LogTALibPath = "../FinanceData/LogTALib.txt"
ISOTIMEFORMAT = '%Y-%m-%d %H:%M:%S'


def CODE1010001( data ):
    theTime = datetime.datetime.now().strftime( ISOTIMEFORMAT )
    logData = theTime + "   CODE1010001: 訓練用的影像太少，低於100000張，目前只有 " + data
    print( "WARNING: ", logData )

    with open( LogFilePath, 'a' ) as outfile:
        outfile.writelines( logData )


def CODE1010002():
    theTime = datetime.datetime.now().strftime( ISOTIMEFORMAT )
    logData = theTime + "   CODE1010002: "
    with open( LogFilePath, 'a' ) as outfile:
        outfile.writelines( logData )


def CODE1010003():
    theTime = datetime.datetime.now().strftime( ISOTIMEFORMAT )
    logData = theTime + "   CODE1010003: "
    with open( LogFilePath, 'a' ) as outfile:
        outfile.writelines( logData )


def CODE1010004():
    theTime = datetime.datetime.now().strftime( ISOTIMEFORMAT )
    logData = theTime + "   CODE1010004: "
    with open( LogFilePath, 'a' ) as outfile:
        outfile.writelines( logData )


def CODE1010005():
    theTime = datetime.datetime.now().strftime( ISOTIMEFORMAT )
    logData = theTime + "   CODE1010005: "
    with open( LogFilePath, 'a' ) as outfile:
        outfile.writelines( logData )


def CODE1010006():
    theTime = datetime.datetime.now().strftime( ISOTIMEFORMAT )
    logData = theTime + "   CODE1010006: "
    with open( LogFilePath, 'a' ) as outfile:
        outfile.writelines( logData )


def CODE1010007():
    theTime = datetime.datetime.now().strftime( ISOTIMEFORMAT )
    logData = theTime + "   CODE1010007: "
    with open( LogFilePath, 'a' ) as outfile:
        outfile.writelines( logData )


def CODE1010008():
    theTime = datetime.datetime.now().strftime( ISOTIMEFORMAT )
    logData = theTime + "   CODE1010008: "
    with open( LogFilePath, 'a' ) as outfile:
        outfile.writelines( logData )


def Default( data ):
    theTime = datetime.datetime.now().strftime( ISOTIMEFORMAT )
    logData = theTime + "   Log Data: " + data + "\n"
    print( logData )
    with open( LogFilePath, 'a' ) as outfile:
        outfile.writelines( logData )


def TALibLog( data ):
    theTime = datetime.datetime.now().strftime( ISOTIMEFORMAT )
    logData = theTime + "   Log Data: " + data + "\n"
    print( logData )
    with open( LogTALibPath, 'a' ) as outfile:
        outfile.writelines( logData )


switcher = {
    "CODE1010001": CODE1010001,
    "CODE1010002": CODE1010002,
    "CODE1010003": CODE1010003,
    "CODE1010004": CODE1010004,
    "CODE1010005": CODE1010005,
    "CODE1010006": CODE1010006,
    "CODE1010007": CODE1010007,
    "CODE1010008": CODE1010008,
    "TALibLog": TALibLog,
    "Default": Default
}


def LogFile( code, data = "" ):
    if data == "":
        switcher.get( code )()
    else:
        switcher.get( "Default" )( data )
