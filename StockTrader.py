import datetime


class StockTrader():
    # 正向交易員，只做多，不做空，且每次交易都設定為全金額投入
    def __init__(self, Name):
        self.Name = Name
        self.initialMoney = 0.0  # 暫定會設定為該股票初始金額
        self.currentMoney = 0.0  # 當前交易結束後的金額
        self.startPrice = 0.0  # 每次交易入場的股價
        self.endPrice = 0.0  # 每次交易出場的股價
        self.holdStocks = 0.0  # 持有股票張數
        self.winTimes = 0.0  # 勝場, 每次入場賺錢的次數
        self.times = 0  # 歷史交易次數
        self.nowAndInitialRate = 0.0  # 與最初本金的比例
        self.winRate = 0.0  # 勝率, 每次入場賺錢的機率
        self.onTrading = False  # 該交易員是否處於交易狀態
        self.OneTimeTrading = True  # 設定是否一次買賣結束才進行第二次買賣, 預設都是如此
        self.MaxLoseMoney = 0  # 每次回測最大虧損金額
        self.averageHoldDuration = 0  # 每次交易持有時間
        self.startDate = ""  # 開始時間
        self.endDate = ""  # 結束時間

    def SettingOneTradding(self, option):
        self.OneTimeTrading = option

    def SetMaxLoseMoney(self, Price):
        self.MaxLoseMoney = Price

    def GetMaxLoseMoney(self):
        return self.MaxLoseMoney

    def GetFinalRate(self):
        return self.nowAndInitialRate

    def BuyTrade(self, price, date):
        # 保護:避免交易員多次進場沒有出場
        if (self.OneTimeTrading == True) & (self.onTrading == True):
            #             print("WARNING: 本次進場不列入計算，因為前次交易並未出場")
            return 0

        # 每次進場時的設定
        self.startPrice = price
        self.startDate = date
        if self.times == 0:
            self.initialMoney = price
            self.currentMoney = price
            self.holdStocks = 1  # 初始買進1張
            # print("初次交易買進張數: " + str(self.holdStocks) + ", 持有金額: " + str(self.currentMoney))
        else:
            self.holdStocks = self.currentMoney / self.startPrice  # 計算目前金額可以買幾張股票
            # print("買進張數: " + str(self.holdStocks))
            self.currentMoney = 0  # 買了股票沒錢中

        self.times += 1
        self.onTrading = True

    def SellTrade(self, price, date):
        # 保護: 確保該交易員已經進場了才能出場
        if (self.OneTimeTrading == True) & (self.onTrading == False):
            # print("WARNING: 本次出場不列入計算，因為前次交易並未入場")
            return 0

        # 計算該次操作的結果
        self.endPrice = price
        self.endDate = date
        if self.endPrice >= self.startPrice:
            self.winTimes += 1

        self.currentMoney = self.endPrice * self.holdStocks  # 取得賣掉股票的錢
        self.currentMoney = self.currentMoney - (
                self.startPrice + self.endPrice) * 0.001425 * self.holdStocks - self.endPrice * self.holdStocks * 0.003
        self.nowAndInitialRate = self.currentMoney / self.initialMoney * 100
        self.winRate = self.winTimes / self.times * 100

        # if self.endPrice > self.startPrice:
        #     print("獲利單次操作: " + str((self.endPrice - self.startPrice)/self.startPrice*100) + "%" + "目前持有金額: " + str(self.currentMoney))
        # else:
        #     print("**虧損單次操作: " + str((self.endPrice - self.startPrice)/self.startPrice*100) + "%""目前持有金額: " + str(self.currentMoney))
        try:
            tmpStartDate = datetime.datetime.strptime(self.startDate, '%Y/%m/%d')
            tmpEndDate = datetime.datetime.strptime(self.endDate, '%Y/%m/%d')
        except:
            tmpStartDate = datetime.datetime.strptime(self.startDate, '%Y-%m-%d')
            tmpEndDate = datetime.datetime.strptime(self.endDate, '%Y-%m-%d')

        interval = tmpEndDate - tmpStartDate
        self.averageHoldDuration = (self.averageHoldDuration * (self.times - 1) + interval.days) / self.times
        self.onTrading = False

    def calculateCurrentProfitRate(self, inputValue):
        return (inputValue - self.startPrice) / self.startPrice

    def printReport(self):
        print("=================Report==================")
        print("交易員稱號: " + self.Name)
        print("初始金額: " + str(self.initialMoney))
        print("交易次數: " + str(self.times))
        print("勝利次數: " + str(self.winTimes))
        print("平均持有天數:" + str(self.averageHoldDuration))
        print("勝　　率: " + str(self.winRate) + "%")
        print("最大虧損金額:" + str(self.MaxLoseMoney))
        print("金錢倍數: " + str(self.nowAndInitialRate) + "%")
        print("=========================================")


class FutureTrader():
    # 正向交易員，只做多，不做空，且每次交易都設定為全金額投入
    def __init__(self, Name):
        self.Name = Name
        # 大台參數
        #         self.OneUnitMoney = 90000 # 原始保證金
        #         self.keepMoney = 70000 # 維持保證金 70000 一口價格
        #         self.OnePointPrice = 200 # 一點的價格
        # 小台參數
        self.OneUnitMoney = 22750  # 原始保證金
        self.keepMoney = 17500  # 維持保證金 70000 一口價格
        self.OnePointPrice = 50  # 一點的價格

        self.holdFeatures = 1  # 持有口數
        self.initialMoney = self.OneUnitMoney * self.holdFeatures  # 初始持有總金額
        self.currentMoney = self.initialMoney  # 當前交易結束後的金額
        self.startPrice = 0.0  # 每次交易入場的股價
        self.endPrice = 0.0  # 每次交易出場的股價
        self.winTimes = 0.0  # 勝場, 每次入場賺錢的次數
        self.times = 0  # 歷史交易次數
        self.MoneyRate = 0.0  # 與最初本金的比例
        self.winRate = 0.0  # 勝率, 每次入場賺錢的機率
        self.onTrading = False  # 該交易員是否處於交易狀態
        self.OneTimeTrading = True  # 設定是否一次買賣結束才進行第二次買賣, 預設都是如此
        self.MaxLoseMoney = 0  # 每次回測最大虧損金額
        self.averageHoldDuration = 0  # 每次交易持有時間
        self.startDate = ""  # 開始時間
        self.endDate = ""  # 結束時間
        self.BROKE_Threshold = self.keepMoney * self.holdFeatures  # 破產金額臨界值
        self.BROKE = False  # 破產宣告

    def SettingOneTradding(self, option):
        self.OneTimeTrading = option

    def SetMaxLoseMoney(self, Price):
        self.MaxLoseMoney = Price

    def GetMaxLoseMoney(self):
        return self.MaxLoseMoney

    def GetCurrentMoney(self):
        return self.currentMoney

    def BuyTrade(self, price, date):
        # 保護:避免交易員多次進場沒有出場
        if (self.OneTimeTrading == True) & (self.onTrading == True):
            print("WARNING: 本次進場不列入計算，因為前次交易並未出場")
            return 0
        # 破產禁止再做交易
        if (self.BROKE == True):
            print("~~WARNING: 已經破產禁止買賣")
            return 0

        self.startPrice = price
        self.startDate = date
        self.times += 1
        self.onTrading = True

    def SellTrade(self, price, date):
        # 保護: 確保該交易員已經進場了才能出場
        if (self.OneTimeTrading == True) & (self.onTrading == False):
            print("WARNING: 本次出場不列入計算，因為前次交易並未入場")
            return 0

        # 破產禁止再做交易
        if (self.BROKE == True):
            #             print("~~WARNING: 已經破產禁止買賣")
            return 0

        # 計算該次操作的結果
        self.endPrice = price
        self.endDate = date
        if self.endPrice >= self.startPrice:
            self.winTimes += 1

        ProfitRate = (self.currentMoney + (
                int(self.endPrice - self.startPrice) * self.OnePointPrice * self.holdFeatures)) / self.currentMoney
        profitMoney = self.currentMoney + int(self.endPrice - self.startPrice) * self.OnePointPrice * self.holdFeatures
        self.currentMoney = profitMoney
        #         print("BUY: "+ str(self.startPrice) + ", SELL: " + str(self.endPrice))
        #         print("ProfitRate: "+ str(ProfitRate) + ", profitMoney: " + str(profitMoney))
        #         if self.endPrice > self.startPrice:
        #             print("獲利單次操作: " + str(ProfitRate) + "%" + "目前持有金額: " + str(self.currentMoney))
        #         else:
        #             print("**虧損單次操作: " + str(ProfitRate) + "%""目前持有金額: " + str(self.currentMoney))

        self.MoneyRate = self.currentMoney / self.initialMoney * 100
        self.winRate = self.winTimes / self.times * 100

        #         if self.currentMoney < self.BROKE_Threshold:
        #             print("~~WARNING: 開始破產")
        #             self.BROKE = True
        #             return 0
        tmpStartDate = datetime.datetime.strptime(self.startDate, '%Y/%m/%d')
        tmpEndDate = datetime.datetime.strptime(self.endDate, '%Y/%m/%d')
        interval = tmpEndDate - tmpStartDate
        self.averageHoldDuration = (self.averageHoldDuration * (self.times - 1) + interval.days) / self.times
        self.onTrading = False

    def calculateCurrentProfitRate(self, inputValue):
        return (inputValue - self.startPrice) / self.startPrice

    def printReport(self):
        if (self.BROKE == True):
            print("===============台期操作報告(破產)===============")
        else:
            print("=================台期操作報告=================")
        print("交易員稱號: " + self.Name)
        print("初始金額: " + str(self.initialMoney))
        print("持有口數: " + str(self.holdFeatures))
        print("原始保證金: " + str(self.OneUnitMoney * self.holdFeatures))
        print("維持保證金: " + str(self.keepMoney * self.holdFeatures))
        print("交易次數: " + str(self.times))
        print("勝利次數: " + str(self.winTimes))
        print("平均持有天數: " + str(self.averageHoldDuration))
        print("勝　　率: " + str(self.winRate) + "%")
        print("最大虧損金額:" + str(self.MaxLoseMoney))
        print("目前金額: " + str(self.currentMoney))
        print("金錢倍數: " + str(self.MoneyRate) + "%")
        print("=============================================")
