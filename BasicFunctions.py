import datetime
import numpy as np

def ConvertDFtoJSON(df):
    output = {}
    for key in df.columns:
        output.update({key:df[key].tolist()})

    return output

# [工具] 輸入JSON最終日期，回傳是否需要更新資料
def checkUpdateOrNot(updateDate):
    updateDate_object = datetime.datetime.strptime(updateDate, '%Y-%m-%d').date()
    nowDate = datetime.date.today()
    diffDay = nowDate - updateDate_object

    # 今天周間，但是今天已經更新過了
    if (updateDate_object.isoweekday() <= 5) & (diffDay.days == 0):
        return False
    # 資料庫要更新的日期是週六週日，且與最後一天的差距兩天以下，就不做更新
    if (nowDate.isoweekday() >= 6) & (diffDay.days <= 2):
        return False

    # 其餘狀況均更新資料
    return True

def OnlineStockToList(stock):
    date_list = [datetime.datetime.strftime(date, '%Y/%m/%d') for date in stock.date]
    closeData = stock.close
    openData = stock.open
    highData = stock.high
    lowData = stock.low
    volume = (np.array(stock.capacity) / 1000).tolist()
    return date_list, closeData, openData, highData, lowData, volume