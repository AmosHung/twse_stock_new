import numpy as np
import pandas as pd
import datetime
import json
import os
import time
import shutil
import loguru

import twstock as TW
import pathSets as PSETs
import BasicFunctions as BF
import LogFile


# 清除所有資料
def RemoveAllDate():
    shutil.rmtree(PSETs.sROOT)

# 生成要分析的目標代碼，包含上市櫃以及期貨
def UpdateTargetCode():
    # 上市櫃更新
    TW.__update_codes()
    stockCode = TW.codes
    output = []
    outputStart = []
    for stock in stockCode:
        strCodeCFI = stockCode[stock].CFI
        if PSETs.targetCodeCFI.find(strCodeCFI) < 0:
            continue
        else:
            stockData = {"Code": stockCode[stock].code,
                         "StartDate": stockCode[stock].start}
            output.append(stockData)
    if not os.path.exists(os.path.dirname(PSETs.sSaveTargetCode)):
        os.makedirs(os.path.dirname(PSETs.sSaveTargetCode))
    json.dump(output, open(PSETs.sSaveTargetCode, "w"), ensure_ascii=False)

    return output


# 撈取個股歷史資料到今天
def GetHistoryData():
    if not os.path.exists(PSETs.sSaveTargetCode):
        print("ERROR:" + PSETs.sSaveTargetCode + " 股號檔案不存在")
        return 0

    # 讀取待抓取的股號列表
    with open(PSETs.sSaveTargetCode) as f:
        stockCode = json.load(f)

    # 確定要存放目標檔案的資料夾存在
    if not os.path.exists(PSETs.sStocksMainPath):
        os.makedirs(PSETs.sStocksMainPath)

    for data in stockCode:
        pathSaveStock = PSETs.sStocksMainPath + data["Code"] + ".json"
        # 如果檔案不存在
        if not os.path.exists(pathSaveStock):
            loguru.logger.info(data["Code"] + "，start from " + data["StartDate"])
            listDate = data["StartDate"].split(r'/')
            # 從起始月份開始抓資料
            stock = TW.Stock(data["Code"])
            stock.fetch_from(int(listDate[0]), int(listDate[1]))
            date_list, closeData, openData, highData, lowData, volume = BF.OnlineStockToList(stock)

            stockSaveData = {"date": date_list,
                             "open": openData,
                             "high": highData,
                             "low": lowData,
                             "close": closeData,
                             "volume": volume}
            json.dump(stockSaveData, open(pathSaveStock, "w"), ensure_ascii=False)
        # 如果檔案存在，檢查最後月份，更新至最新月份
        else:
            # 讀取已存在的股號檔案
            with open(pathSaveStock) as f:
                stockExist = json.load(f)
            finalDate = stockExist["date"][-1]

            # 如果無須更新資料，就跳過這個stock
            if not BF.checkUpdateOrNot(finalDate):
                loguru.logger.success(data["Code"] + "，is exist and data is newest")
                continue
            # 如果要更新資料，讀取即時資料
            else:
                loguru.logger.info(data["Code"] + "，is exist and update the last month data")
                finalMonth = finalDate[:-3]
                # tStart = time.time()  # 計時開始
                for i in range(1, len(stockExist["date"]) + 1):
                    if stockExist["date"][-1].find(finalMonth) >= 0:
                        stockExist["date"].pop()
                        stockExist["open"].pop()
                        stockExist["high"].pop()
                        stockExist["close"].pop()
                        stockExist["low"].pop()
                        stockExist["volume"].pop()
                    else:
                        break
                # tEnd = time.time()  # 計時結束
                # print("It cost %f sec" % (tEnd - tStart))  # 會自動做近位

                listDate = finalMonth.split(r'/')
                stock = TW.Stock(data["Code"])
                stock.fetch_from(int(listDate[0]), int(listDate[1]))
                date_list, closeData, openData, highData, lowData, volume = BF.OnlineStockToList(stock)

                stockSaveData = {"date": stockExist["date"] + date_list,
                                 "open": stockExist["open"] + openData,
                                 "high": stockExist["high"] + highData,
                                 "low": stockExist["low"] + lowData,
                                 "close": stockExist["close"] + closeData,
                                 "volume": stockExist["volume"] + volume}

                json.dump(stockSaveData, open(pathSaveStock, "w"), ensure_ascii=False)

            return 0

    return 0


# 抓最新一天的個股資料，整合到舊有的個股資料中
def GetUpdateData():
    return 0
