sROOT = r"../FinanceData/"

# region constant variable
targetCodeCFI = 'ESVUFR|ESVTFR|CEOGEU|CEOGDU|CEOGMU|CEOIBU|CEOJEU|CEOIEU|CEOIBU|CEOJLU|CEOIRU'
# endregion

# region STOCK & OTC
sSaveTargetCode = r"../FinanceData/TargetCode.json"
sStocksMainPath = r'../FinanceData/Targets/'
sStocksTechPath = r'../FinanceData/TechIndicators/'
#endregion

# region Futures
sWorldFutureWeb = "https://finance.yahoo.com/world-indices/"
sWorldFutureIDPath = r"../FinanceData/WorldFuture_Name.csv"
sFutureMainPath = r"../FinanceData/WorldFutures/"
#endregion

#region TALib
sTALIBPath = r"../FinanceData/TA_Data/"
sTALIB_STOCKPath = r"../FinanceData/TA_Data/STOCKs/"
sTALIB_OTCPath = r"../FinanceData/TA_Data/OTCs/"
sTALIB_FUTUREPath = r"../FinanceData/TA_Data/Futures/"
#endregion

# region Evaluator
sEVALPath = r"../FinanceData/BackTest/"
sEVAL_PIC_STOCKPath = r"../FinanceData/BackTest/STOCK_OTC/PIC_STOCK/"
sEVAL_PIC_OTCPath = r"../FinanceData/BackTest/STOCK_OTC/PIC_OTC/"
sEVAL_PIC_FUTUREPath = r"../FinanceData/BackTest/FUTURE/PIC_FUTURE/"
sEVAL_STOCKOTC_FinalReport = r"../FinanceData/STOCK_OTC/"
sEVAL_FUTURE_FinalReport = r"../FinanceData/FUTURE/"
sEVAL_STOCKAROONTimeZone = r"../FinanceData/STOCK_AROONTime.csv"
sEVAL_OTCAROONTimeZone = r"../FinanceData/OTC_AROONTime.csv"
sEVAL_FUTUREAROONTimeZone = r"../FinanceData/BackTest/FUTURE_AROONTime.csv"
# endregion

#region Prediction Close 預測收盤價
sPREDICTIONPath = r"../FinanceData/Prediction/"
sPRECTION_STOCKPath = r"../FinanceData/Prediction/STOCKs/"
sPRECTION_OTCPath = r"../FinanceData/Prediction/OTCs/"
sPRECTION_FUTUREPath = r"../FinanceData/Prediction/Futures/"
# endregion