import abc

from itertools import cycle
import time
import loguru
import pyquery
import requests


class ProxyProvider(abc.ABC):
    @abc.abstractmethod
    def get_proxy(self):
        return NotImplemented


class NoProxyProvier():
    def get_proxy(self):
        return {}

class RoundRobinProxiesProvider():
    def __init__(self):
        proxies, proxies_https = self.getProxiesFromFreeProxyList()
        self._proxies = proxies
        self._proxiesHTTPS = proxies_https
        self._proxies_cycle = cycle(proxies)
        self._proxiesHTTPS_cycle = cycle(proxies_https)
        # 每使用timeLimitation次之後，要重新更新proxylist
        self.timeLimitation = 600
        self.times = 0

    @property
    def proxies(self):
        return self._proxies, self._proxiesHTTPS

    @proxies.setter
    def proxies(self, proxies: list, proxies_https: list):
        if not isinstance(proxies, list):
            raise ValueError('Proxies only accept list')
        if not isinstance(proxies_https, list):
            raise ValueError('proxies_https only accept list')

        self._proxies = proxies
        self._proxiesHTTPS = proxies_https
        self._proxies_cycle = cycle(proxies)
        self._proxiesHTTPS_cycle = cycle(proxies_https)

    def get_proxy(self):
        if self.times > self.timeLimitation:
            self.times = 0
            pp, pps = self.getProxiesFromFreeProxyList()
            self.proxies(pp, pps)
            tmpProxy = next(self._proxies_cycle)
            tmpProxy_HTTPS = next(self._proxiesHTTPS_cycle)
            return {"http": tmpProxy, "https": tmpProxy_HTTPS}
        else:
            self.times += 1
            tmpProxy = next(self._proxies_cycle)
            tmpProxy_HTTPS = next(self._proxiesHTTPS_cycle)
            return {"http": tmpProxy, "https": tmpProxy_HTTPS}

    def getProxiesFromFreeProxyList(self):
        proxies = []
        proxies_https = []
        url = 'https://free-proxy-list.net/'
        loguru.logger.debug(f'getProxiesFromFreeProxyList: {url}')
        loguru.logger.warning(f'getProxiesFromFreeProxyList: downloading...')
        response = requests.get(url)
        if response.status_code != 200:
            loguru.logger.debug(f'getProxiesFromFreeProxyList: status code is not 200')
            return
        loguru.logger.success(f'getProxiesFromFreeProxyList: downloaded.')
        d = pyquery.PyQuery(response.text)
        trs = list(d('table#proxylisttable > tbody > tr').items())
        loguru.logger.warning(f'getProxiesFromFreeProxyList: scanning...')
        for tr in trs:
            # 取出所有資料格
            tds = list(tr('td').items())
            # 取出 IP 欄位值
            ip = tds[0].text().strip()
            # 取出 Port 欄位值
            port = tds[1].text().strip()
            # 取得elite
            elite = tds[4].text().strip()
            # 區分是否https
            httpsD = tds[6].text().strip()
            # 組合 IP 代理
            if elite.find('elite') >= 0:
                if httpsD.find('yes') >= 0:
                    proxy = f'{ip}:{port}'
                    proxies_https.append(proxy)
                if httpsD.find('no') >= 0:
                    proxy = f'{ip}:{port}'
                    proxies.append(proxy)

        loguru.logger.success(f'getProxiesFromFreeProxyList: scanned.')
        loguru.logger.debug(f'getProxiesFromFreeProxyList: {len(proxies) + len(proxies_https)} proxies is found.')
        return proxies, proxies_https


_provider_instance = NoProxyProvier()


def get_proxies():
    return _provider_instance.get_proxy()
