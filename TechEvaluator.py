import numpy as np
import pandas as pd
import json
import os
import talib
import loguru

import pathSets as PSETs
import LogFile
import StockTrader
import BasicFunctions as BF


def TA_AROON( basicData, timeZone ):
    aroonOSC = talib.AROONOSC( basicData[ "HA_High" ], basicData[ "HA_Low" ], timeperiod = timeZone )
    aroondown, aroonup = talib.AROON( basicData[ "HA_High" ], basicData[ "HA_Low" ], timeperiod = timeZone )

    output = { "AROONUP": aroonup,
               "AROONDOWN": aroondown,
               "AROONOSC": aroonOSC,
               "date": basicData[ "date" ] }
    return output


def TA_BBAND( close, date, timeperiod = 25, nbdevup = 2, nbdevdn = 2, matype = 1 ):
    upperband, middleband, lowerband = talib.BBANDS( close,
                                                     timeperiod = timeperiod,
                                                     nbdevup = nbdevup,
                                                     nbdevdn = nbdevdn,
                                                     matype = matype )
    B_PB = (close - lowerband) / (upperband - lowerband)
    B_Width = (upperband - lowerband) / middleband
    output = { "B_Upper": upperband.tolist(),
               "B_Middle": middleband.tolist(),
               "B_Lower": lowerband.tolist(),
               "B_%B": B_PB.tolist(),
               "B_Width": B_Width.tolist(),
               "date": date.tolist() }
    return output


def TA_HA( opend, high, low, close, date ):
    ha_close = (opend + high + low + close) / 4
    ha_close = ha_close.tolist()
    ha_open = [ ]
    for i in range( 0, len( opend ) ):
        if i == 0:
            ha_open.append( (opend[ i ] + close[ i ]) / 2 )
        else:
            ha_open.append( (ha_open[ i - 1 ] + ha_close[ i - 1 ]) / 2 )

    tmpArray = np.column_stack( (ha_open, ha_close, high) )
    ha_high = np.max( tmpArray, axis = 1 ).tolist()

    tmpArray = np.column_stack( (ha_open, ha_close, low) )
    ha_low = np.min( tmpArray, axis = 1 ).tolist()

    output = { "HA_Open": ha_open,
               "HA_High": ha_high,
               "HA_Low": ha_low,
               "HA_Close": ha_close,
               "date": date.tolist() }

    return output


def TA_SuperTrend( dataframe, multiplier = 1.5, period = 9 ):
    """
    Supertrend Indicator
    adapted for freqtrade
    from: https://github.com/freqtrade/freqtrade-strategies/issues/30
    """
    df = dataframe.copy()

    opend = df[ "HA_Open" ]
    close = df[ "HA_Close" ]
    high = df[ "HA_High" ]
    low = df[ "HA_Low" ]

    df[ 'ATR' ] = talib.ATR( high, low, close, timeperiod = period )

    # atr = 'ATR_' + str(period)
    st = 'ST'
    stx = 'STX'

    # Compute basic upper and lower bands
    df[ 'basic_ub' ] = (high + low) / 2 + multiplier * df[ 'ATR' ]
    df[ 'basic_lb' ] = (high + low) / 2 - multiplier * df[ 'ATR' ]

    # Compute final upper and lower bands
    df[ 'final_ub' ] = 0.00
    df[ 'final_lb' ] = 0.00
    for i in range( period, len( df ) ):
        df[ 'final_ub' ].iat[ i ] = df[ 'basic_ub' ].iat[ i ] if df[ 'basic_ub' ].iat[ i ] < df[ 'final_ub' ].iat[
            i - 1 ] or close.iat[
                                                                     i - 1 ] > df[ 'final_ub' ].iat[ i - 1 ] else \
            df[ 'final_ub' ].iat[ i - 1 ]
        df[ 'final_lb' ].iat[ i ] = df[ 'basic_lb' ].iat[ i ] if df[ 'basic_lb' ].iat[ i ] > df[ 'final_lb' ].iat[
            i - 1 ] or close.iat[
                                                                     i - 1 ] < df[ 'final_lb' ].iat[ i - 1 ] else \
            df[ 'final_lb' ].iat[ i - 1 ]

    # Set the Supertrend value
    df[ st ] = 0.00
    for i in range( period, len( df ) ):
        df[ st ].iat[ i ] = df[ 'final_ub' ].iat[ i ] if df[ st ].iat[ i - 1 ] == df[ 'final_ub' ].iat[ i - 1 ] and \
                                                         close.iat[ i ] <= \
                                                         df[ 'final_ub' ].iat[ i ] else \
            df[ 'final_lb' ].iat[ i ] if df[ st ].iat[ i - 1 ] == df[ 'final_ub' ].iat[ i - 1 ] and close.iat[ i ] > \
                                         df[ 'final_ub' ].iat[ i ] else \
                df[ 'final_lb' ].iat[ i ] if df[ st ].iat[ i - 1 ] == df[ 'final_lb' ].iat[ i - 1 ] and close.iat[
                    i ] >= \
                                             df[ 'final_lb' ].iat[ i ] else \
                    df[ 'final_ub' ].iat[ i ] if df[ st ].iat[ i - 1 ] == df[ 'final_lb' ].iat[ i - 1 ] and close.iat[
                        i ] < \
                                                 df[ 'final_lb' ].iat[ i ] else 0.00

        # Mark the trend direction up/down
    df[ stx ] = np.where( (df[ st ] > 0.00), np.where( (close < df[ st ]), 'down', 'up' ), np.NaN )
    output = { "ST": df[ st ].tolist(),
               "STX": df[ stx ].tolist(),
               "date": df[ "date" ].tolist() }

    return output


# 用策略回測，找出各個股票適合的時間區間
def StrategicEvaluation( df, trader, timeperiod = 14 ):
    close = np.array( df[ "close" ] )
    high = np.array( df[ "HA_High" ] )
    low = np.array( df[ "HA_Low" ] )
    opend = np.array( df[ "open" ] )

    date = np.array( df[ "date" ] )
    aroonOSC = np.array( talib.AROONOSC( high, low, timeperiod = timeperiod ) )

    for i in range( len( aroonOSC ) ):
        if i < 2:
            continue
        if (aroonOSC[ i - 1 ] < 0) & (aroonOSC[ i ] > 0):
            trader.BuyTrade( close[ i ], date[ i ] )
        #             print("BUY:",date[i])
        if (aroonOSC[ i - 1 ] > 0) & (aroonOSC[ i ] < 0):
            trader.SellTrade( opend[ i ], date[ i ] )

        # 最後一天如果股票仍持有，賣掉好計算至今的獲利
        if i == (len( aroonOSC ) - 1):
            trader.SellTrade( opend[ i ], date[ i ] )

    #             print("SELL:",date[i])

    return 0


def CalculateTechIndicator():
    if not os.path.exists( PSETs.sSaveTargetCode ):
        print( "ERROR:" + PSETs.sSaveTargetCode + " 股號檔案不存在" )
        return 0
    # 確定要存放的指標資料夾存在
    if not os.path.exists( PSETs.sStocksTechPath ):
        os.makedirs( PSETs.sStocksTechPath )

    # 讀取待抓取的股號列表
    with open( PSETs.sSaveTargetCode ) as f:
        stockCode = json.load( f )

    # 確定要存放目標檔案的資料夾存在
    if not os.path.exists( PSETs.sStocksMainPath ):
        os.makedirs( PSETs.sStocksMainPath )

    for data in stockCode:
        pathLoadStock = PSETs.sStocksMainPath + data[ "Code" ] + ".json"
        pathSaveStock = PSETs.sStocksTechPath +  data[ "Code" ] + ".json"
        if os.path.exists( pathLoadStock ):
            loguru.logger.info(data["Code"] + "，transform JSON to DATAFRAME")
            csv_ori = pd.read_json( pathLoadStock, convert_dates = False )
        else:
            loguru.logger.error(data["Code"] + "，file doesn't exist")
            continue
        # 確保資料裡面沒有NaN
        csv_ori.dropna( inplace = True )

        # 計算平均K線
        dataHA = TA_HA( csv_ori[ "open" ], csv_ori[ "high" ], csv_ori[ "low" ], csv_ori[ "close" ], csv_ori[ "date" ] )
        csv_HA = pd.DataFrame.from_dict( dataHA )

        # 計算布林通道
        dataBB = TA_BBAND( csv_ori[ "close" ], csv_ori[ "date" ] )
        csv_BB = pd.DataFrame.from_dict( dataBB )

        # 基於平均K，取得適合的阿隆參數
        basicData = { "HA_High": csv_HA[ "HA_High" ],
                      "HA_Low": csv_HA[ "HA_Low" ],
                      "close": csv_ori[ "close" ],
                      "open": csv_ori[ "open" ],
                      "date": csv_ori[ "date" ] }
        totalAROONProfit = [ ]
        totalAROONIndex = [ ]
        for i in range( 10 ):
            tperiod = 14 + i
            trader = StockTrader.StockTrader( "AROON" )
            StrategicEvaluation( basicData, trader, tperiod )
            # trader.printReport()
            totalAROONProfit.append( trader.GetFinalRate() )
            totalAROONIndex.append( tperiod )

        # Get the indices of maximum element in numpy array
        result = np.where( totalAROONProfit == np.amax( totalAROONProfit ) )
        aroonPeriod = totalAROONIndex[ int( result[ 0 ] ) ]
        dataAroon = TA_AROON( basicData, aroonPeriod )
        csv_AROON = pd.DataFrame.from_dict( dataAroon )

        # 基於平均K，取得適合的超級趨勢
        dataST = TA_SuperTrend( csv_HA )
        csv_ST = pd.DataFrame.from_dict( dataST )

        # 基於日期整合所有DataFrame
        csv_final = pd.merge( csv_ori, csv_HA, on = 'date' )
        csv_final = pd.merge( csv_final, csv_BB, on = 'date' )
        csv_final = pd.merge( csv_final, csv_AROON, on = 'date' )
        csv_final = pd.merge( csv_final, csv_ST, on = 'date' )

        csv_final.info()
        csv_final.dropna( inplace = True )
        csv_final.reset_index( drop = True, inplace = True )
        csv_final.info()

        outputJSON = BF.ConvertDFtoJSON( csv_final )
        json.dump( outputJSON, open( pathSaveStock, "w" ), ensure_ascii = False )

    return 0


# 畫圖
def plot_candles( pricing, stockid, title = None, volume_bars = False, color_function = None, technicals = None,
                  enableHA = True ):
    """ Plots a candlestick chart using quantopian pricing data.

    Author: Daniel Treiman

    Args:
      pricing: A pandas dataframe with columns ['open_price', 'close_price', 'high', 'low', 'volume']
      title: An optional title for the chart
      volume_bars: If True, plots volume bars
      color_function: A function which, given a row index and price series, returns a candle color.
      technicals: A list of additional data series to add to the chart.  Must be the same length as pricing.
    """
    try:
        pricing.set_index( 'date', inplace = True )
    except:
        print( "已經是用Date當作index了" )

    def default_color( index, open_price, close_price, low, high ):
        return 'g' if open_price.at[ index ] > close_price.at[ index ] else 'r'

    color_function = default_color
    technicals = technicals or [ ]

    opend = pricing[ 'HA_Open' ]
    close = pricing[ 'HA_Close' ]
    low = pricing[ 'HA_Low' ]
    high = pricing[ 'HA_High' ]

    oc_min = pd.concat( [ opend, close ], axis = 1 ).min( axis = 1 )
    oc_max = pd.concat( [ opend, close ], axis = 1 ).max( axis = 1 )
    deltaOC = oc_max - oc_min
    deltaOC = deltaOC.mask( deltaOC < 0.01 ).fillna( 0.02 )

    fig, (ax1, ax2, ax3, ax4, ax5) = plt.subplots( 5, 1, sharex = True,
                                                   gridspec_kw = { 'height_ratios': [ 3, 3, 1, 1, 1 ] },
                                                   figsize = (16, 12) )
    ax1.set_title( stockid )

    x = pricing.index
    candle_colors = [ color_function( i, opend, close, low, high ) for i in x ]
    candles = ax1.bar( x, height = deltaOC, width = 0.8, bottom = oc_min - 0.02, color = candle_colors )
    lines = ax1.vlines( x, ymin = low, ymax = high, color = candle_colors, linestyles = 'solid' )
    ax1.xaxis.grid( False )
    ax1.xaxis.set_tick_params( which = 'major', length = 3.0, direction = 'in', top = 'off' )
    time_format = '%Y/%m/%d'

    # axis2: 原始股價圖
    open_ori = pricing[ 'open' ]
    close_ori = pricing[ 'close' ]
    low_ori = pricing[ 'low' ]
    high_ori = pricing[ 'high' ]
    candle_colors = [ color_function( i, open_ori, close_ori, low_ori, high_ori ) for i in x ]
    oc_min = pd.concat( [ open_ori, close_ori ], axis = 1 ).min( axis = 1 )
    oc_max = pd.concat( [ open_ori, close_ori ], axis = 1 ).max( axis = 1 )
    deltaOC = oc_max - oc_min
    deltaOC = deltaOC.mask( deltaOC < 0.01 ).fillna( 0.02 )

    candles = ax2.bar( x, height = deltaOC, width = 0.8, bottom = oc_min - 0.02, color = candle_colors )
    lines = ax2.vlines( x, ymin = low, ymax = high, color = candle_colors, linestyles = 'solid' )
    ax2.xaxis.grid( False )
    ax2.xaxis.set_tick_params( which = 'major', length = 3.0, direction = 'in', top = 'off' )

    dst = pricing[ "ST" ]
    bUpper = pricing[ "STX" ] == 'up'
    bLower = pricing[ "STX" ] == 'down'

    ax2.plot( dst.mask( bUpper ), color = '#0029FA', label = "pressure" )
    ax2.plot( dst.mask( bLower ), color = '#A6035D', label = "support" )
    upperband = pricing[ "B_Upper" ]
    middleband = pricing[ "B_Middle" ]
    lowerband = pricing[ "B_Lower" ]

    ax2.plot( upperband, label = "b_up", color = 'gray', linestyle = ':' )
    ax2.plot( middleband, label = "b_mid", color = 'gray', linestyle = ':' )
    ax2.plot( lowerband, label = "b_low", color = 'gray', linestyle = ':' )
    #     ax2.set_xticks(range(0, len(pricing.index), 10))
    #     ax2.set_xticklabels(pricing.index[::10],rotation=90)

    # axis3: 成交量
    volume = pricing[ 'volume' ]
    volume_scale = None
    scaled_volume = volume
    if volume.max() > 1000000:
        volume_scale = 'M'
        scaled_volume = volume / 1000000
    elif volume.max() > 1000:
        volume_scale = 'K'
        scaled_volume = volume / 1000
    ax3.bar( x, scaled_volume, color = candle_colors )
    volume_title = 'Volume'
    if volume_scale:
        volume_title = 'Volume (%s)' % volume_scale
    ax3.set_title( volume_title )
    ax3.xaxis.grid( False )

    # axis4: AROON UP and DOWN
    aUp = pricing[ "AROONUP" ]
    aDown = pricing[ "AROONDOWN" ]
    ax4.plot( x, aUp, label = "AROONUP", color = '#F20544' )
    ax4.plot( x, aDown, label = "AROONDOWN", color = '#5B7343' )
    ax4.axhline( 50, color = "gray", linestyle = ':' )
    #     ax4.set_xticks(range(0, len(pricing.index), 10))
    #     ax4.set_xticklabels(pricing.index[::10],rotation=90)

    #     # axis4: AROON OSC
    #     plt.plot(pricing["AROONOSC"])
    aOSC = pricing[ "AROONOSC" ]
    ax5.plot( x, aOSC, label = "AROONOSC", color = '#230A59' )
    ax5.axhline( 0, color = "gray", linestyle = '-' )

    ax5.set_xticks( range( 0, len( pricing.index ), 10 ) )
    ax5.set_xticklabels( pricing.index[ ::10 ], rotation = 90 )

    #     ax1.legend(loc='lower left')
    ax2.legend( loc = 'lower left' )
    ax4.legend( loc = 'lower left' )
    ax5.legend( loc = 'lower left' )


def AdviceAndPlot():
    if not os.path.exists( PSETs.sSaveTargetCode ):
        print( "ERROR:" + PSETs.sSaveTargetCode + " 股號檔案不存在" )
        return 0

    # 讀取待抓取的股號列表
    with open( PSETs.sSaveTargetCode ) as f:
        stockCode = json.load( f )

    # 確定要存放目標檔案的資料夾存在
    if not os.path.exists( PSETs.sStocksMainPath ):
        os.makedirs( PSETs.sStocksMainPath )

    for data in stockCode:
        pathLoadStock = PSETs.sStocksMainPath + data[ "Code" ] + ".json"
        # 如果檔案不存在
        if not os.path.exists( pathLoadStock ):
            loguru.logger.error(data["Code"] + "，plot file, but file doesn't exist")
            continue
        else:
            # 讀取已存在的股號檔案
            with open( pathLoadStock ) as f:
                stockExist = json.load( f )

    return 0
